<?php

class Ape extends Animal
{
    function __construct($nama)
    {
        parent::__construct($nama);
        $this->legs = 2;
        $this->cold_blooded = "no";
    }
    function Yell()
    {
        return "Auooo";
    }
    function Display()
    {
        $yell = $this->Yell();
        $br = "<br>";
        echo "Name: $this->name{$br}Legs: $this->legs{$br}Cold Blooded: $this->cold_blooded{$br}Yell: $yell{$br}";
    }
}
?>