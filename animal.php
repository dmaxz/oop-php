<?php
class Animal
{
    protected $name;
    protected $legs;
    protected $cold_blooded;

    function __construct($nama)
    {
     $this->name = $nama;
     $this->legs = 4;
     $this->cold_blooded = "no";  
    }
    
    function GetName()
    {   $br = "<br>";
        echo $this->name . $br; // "shaun"

    }

    function GetLegs()
    {   $br = "<br>";

        echo $this->legs . $br; // 4

    }

    function GetColdBlooded()
    {   $br = "<br>";


        echo $this->cold_blooded . $br; // "no"}
    }

    function Display()
    {
        $br = "<br>";
        echo "Name: $this->name{$br}Legs: $this->legs{$br}Cold Blooded: $this->cold_blooded{$br}";
    }
}


?>