<?php

class Frog extends Animal
{
    function __construct($nama)
    {
        parent::__construct($nama);
        $this->legs = 4;
        $this->cold_blooded = "yes";
    }
    function Jump()
    {
        return "Hop Hop";
    }
    function Display()
    {
        $jump = $this->Jump();
        $br = "<br>";
        echo "Name: $this->name{$br}Legs: $this->legs{$br}Cold Blooded: $this->cold_blooded{$br}Jump: $jump{$br}";
    }
}
?>