<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>

<body>
    <?php
    require 'animal.php';
    require 'ape.php';
    require 'frog.php';

    $br = "<br>";
    $sheep = new Animal("shaun");

    echo "<h2>Release 0</h2>";

    $sheep->GetName();
    $sheep->GetLegs();
    $sheep->GetColdBlooded();

    echo $br;
    // NB: Boleh juga menggunakan method get (get_name(), get_legs(), get_cold_blooded())

    echo "<h2>Release 1</h2>";

    // index.php
    $sungokong = new Ape("kera sakti");
    echo $sungokong->Yell(); // "Auooo"
    echo $br . $br;

    $kodok = new Frog("buduk");
    echo $kodok->Jump(); // "hop hop"
    echo $br . $br;

    echo "<h4>Using Display() function</h4>";

    $sheep->Display();

    echo $br;

    $kodok->Display();

    echo $br;

    $sungokong->Display();











    ?>
</body>

</html>